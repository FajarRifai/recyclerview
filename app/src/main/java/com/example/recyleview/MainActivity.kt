package com.example.recyleview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    private lateinit var rvHero: RecyclerView
    private var title = "Mode List"
    private val list = ArrayList<SuperHero>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvHero = findViewById(R.id.rv_hero)
        rvHero.setHasFixedSize(true)

        list.addAll(HeroesData.listData)
        showList()
    }
    private fun showList() {
        rvHero.layoutManager = LinearLayoutManager(this)
        val superHeroAdapter = SuperHeroAdapter(list)
        rvHero.adapter = superHeroAdapter

        superHeroAdapter.setOnItemClickCallback(object : SuperHeroAdapter.OnItemClickCallback {
            override fun onItemClicked(data: SuperHero) {
                showSelected(data)
            }
        })
    }

    private fun showGrid() {
        rvHero.layoutManager = GridLayoutManager(this, 2)
        val gridHeroAdapter = GridHeroAdapter(list)
        rvHero.adapter = gridHeroAdapter

        gridHeroAdapter.setOnItemClickCallback(object : GridHeroAdapter.OnItemClickCallback {
            override fun onItemClicked(data: SuperHero) {
                showSelected(data)
            }
        })
    }
    private fun showCardView() {
        rvHero.layoutManager = LinearLayoutManager(this)
        val cardViewHeroAdapter = CardViewAdapter(list)
        rvHero.adapter = cardViewHeroAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_list -> {
                title = "Mode List"
                showList()
                true
            }
            R.id.action_grid -> {
                title = "Mode Grid"
                showGrid()
                true
            }
            R.id.action_cardView -> {
                title = "Mode Card View"
                showCardView()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    private fun showSelected(superHero: SuperHero) {
        Toast.makeText(this, "Kamu memilih " + superHero.nameSuperHero, Toast.LENGTH_SHORT).show()
    }
}
package com.example.recyleview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class CardViewAdapter(private val superhero: ArrayList<SuperHero>)
    :RecyclerView.Adapter<CardViewAdapter.SuperHeroViewHolder>() {


    class SuperHeroViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        var imgSuperHero = itemView.findViewById<ImageView>(R.id.img_item_photo)
        var nameSuperHero = itemView.findViewById<TextView>(R.id.tv_item_name)
        var descSuperHero = itemView.findViewById<TextView>(R.id.tv_item_description)
        var btnFavorite = itemView.findViewById<Button>(R.id.btn_set_favorite)
        var btnShare = itemView.findViewById<Button>(R.id.btn_set_share)

        fun bindView(superhero: SuperHero){
            imgSuperHero.setImageResource(superhero.imgSuperHero)
            nameSuperHero.text = superhero.nameSuperHero
            descSuperHero.text = superhero.descSuperHero
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SuperHeroViewHolder {
        return SuperHeroViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_cardview, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: SuperHeroViewHolder, position: Int) {
        holder.bindView(superhero[position])

        holder.btnFavorite.setOnClickListener { Toast.makeText(holder.itemView.context, "Favorite " + superhero[holder.adapterPosition].nameSuperHero, Toast.LENGTH_SHORT).show() }

        holder.btnShare.setOnClickListener { Toast.makeText(holder.itemView.context, "Share " + superhero[holder.adapterPosition].nameSuperHero, Toast.LENGTH_SHORT).show() }

        holder.itemView.setOnClickListener { Toast.makeText(holder.itemView.context, "Kamu memilih " + superhero[holder.adapterPosition].nameSuperHero, Toast.LENGTH_SHORT).show() }
    }

    override fun getItemCount(): Int = superhero.size
}
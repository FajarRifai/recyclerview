package com.example.recyleview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class SuperHeroAdapter(private val superhero: ArrayList<SuperHero>)
    :RecyclerView.Adapter<SuperHeroAdapter.SuperHeroViewHolder>() {

    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    class SuperHeroViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val imgSuperHero = view.findViewById<ImageView>(R.id.img_item_photo)
        val nameSuperHero = view.findViewById<TextView>(R.id.tv_item_name)
        val descSuperHero = view.findViewById<TextView>(R.id.tv_item_description)

        fun bindView(superhero: SuperHero){
            imgSuperHero.setImageResource(superhero.imgSuperHero)
            nameSuperHero.text = superhero.nameSuperHero
            descSuperHero.text = superhero.descSuperHero
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SuperHeroViewHolder {
        return SuperHeroViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_recyleview, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: SuperHeroViewHolder, position: Int) {
        holder.bindView(superhero[position])

        holder.itemView.setOnClickListener { onItemClickCallback.onItemClicked(superhero[holder.adapterPosition]) }
    }

    override fun getItemCount(): Int = superhero.size

    interface OnItemClickCallback {
        fun onItemClicked(data: SuperHero)
    }
}

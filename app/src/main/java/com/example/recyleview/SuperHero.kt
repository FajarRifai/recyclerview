package com.example.recyleview

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize

data class SuperHero(
    var imgSuperHero: Int = 0,
    var nameSuperHero: String = "",
    var descSuperHero: String = ""
) : Parcelable

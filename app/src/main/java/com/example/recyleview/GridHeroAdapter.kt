package com.example.recyleview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

class GridHeroAdapter(private val superhero: List<SuperHero>)
    : RecyclerView.Adapter<GridHeroAdapter.SuperHeroViewHolder>() {

    private lateinit var onItemClickCallback: OnItemClickCallback

    internal fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    class SuperHeroViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val imgSuperHero = view.findViewById<ImageView>(R.id.img_item_photo)


        fun bindView(superhero: SuperHero){
            imgSuperHero.setImageResource(superhero.imgSuperHero)

        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SuperHeroViewHolder {
        return SuperHeroViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_grid, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: SuperHeroViewHolder, position: Int) {
        holder.bindView(superhero[position])
    }

    override fun getItemCount(): Int = superhero.size

    interface OnItemClickCallback {
        fun onItemClicked(superhero: SuperHero)
    }
}